//
//  ParentViewController.h
//  ejabberdTest
//
//  Created by Volodymyr Viniarskyi on 2/22/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParentViewController : UIViewController
- (void)startActivity;
- (void)stopActivity;
@end
