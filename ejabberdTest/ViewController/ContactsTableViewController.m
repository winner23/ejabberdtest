//
//  ContactsTableViewController.m
//  ejabberTest
//
//  Created by Volodymyr Viniarskyi on 2/12/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import "ContactsTableViewController.h"
#import "Connection.h"
#import "XMPPJID.h"
#import "ChatTableViewController.h"
@interface ContactsTableViewController ()

@property (strong, nonatomic) Connection *connectionManager;
@end

@implementation ContactsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.connectionManager = [Connection connectionManager];
    XMPPJID *jid = self.connectionManager.jid;
    NSString *titleString = [jid user];
    self.navigationItem.title = titleString;
    
    [self.connectionManager getAllUsers];    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.connectionManager.contacts count];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}
- (IBAction)addButton1:(UIBarButtonItem *)sender {
    [self.navigationController performSegueWithIdentifier:@"addContactSegue" sender:self];
}
- (IBAction)addBarButtonTapped:(UIBarButtonItem *)sender {
    [self.navigationController performSegueWithIdentifier:@"addContactSegue" sender:self];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"contactCell" forIndexPath:indexPath];
    if ([self.connectionManager.contacts count] > indexPath.row) {
        XMPPJID *jid = self.connectionManager.contacts[indexPath.row];
        cell.textLabel.text = [jid user];
    }
    
    
    // Configure the cell...
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    chatSegue
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ChatTableViewController *chatTableViewController = [storyboard instantiateViewControllerWithIdentifier:@"ChatTableViewController"];
    chatTableViewController.userName = [self.connectionManager.contacts[indexPath.row] user];
    [self.navigationController pushViewController:chatTableViewController animated:YES];
    
    //[self.navigationController performSegueWithIdentifier:@"chatSegue" sender:self];
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
