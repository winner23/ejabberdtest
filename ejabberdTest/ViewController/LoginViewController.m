//
//  LoginViewController.m
//  ejabberTest
//
//  Created by Volodymyr Viniarskyi on 2/8/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import "LoginViewController.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
//#import <ReactiveCocoa/>
#import "Connection.h"
#import "User.h"
#import "Server.h"
@interface LoginViewController ()
@property (strong, nonatomic) Connection *connectionManager;
@property (weak, nonatomic) IBOutlet UITextField *serverPortTextField;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *jidTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *serverAddressTextField;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *testConnectionButton;
@end
@implementation LoginViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialisation];
    [[UITextField appearance] setTintColor:UIColor.blueColor];
}

- (void) initialisation {
    RAC(self.loginButton, enabled) = [[RACSignal combineLatest: @[
                                                                  self.jidTextField.rac_textSignal,
                                                                  self.passwordTextField.rac_textSignal,
                                                                  self.nameTextField.rac_textSignal]
                                                        reduce:^id(NSString *jidText,
                                                                   NSString *password,
                                                                   NSString *name){
                                                            return @(jidText.length>5 && password.length>3 && name.length>2);
                                                        }] throttle:2];
}
- (void)setUser {
    User *user;
    if (![User sharedInstance]){
        NSLog(@"User instance created!");
        user = [User userWithJID:self.jidTextField.text name:self.nameTextField.text initPass:self.passwordTextField.text];
    } else {
        NSLog(@"User modified!");
        user = [User sharedInstance];
        user.name = self.nameTextField.text;
        user.password = self.passwordTextField.text;
        user.jidString = self.jidTextField.text;
    }
}
- (void)setServer {
    Server *server;
    if (![Server sharedInstace]) {
        NSLog(@"Server instance created!");
        server = [Server serverWithAddress:self.serverAddressTextField.text port:self.serverPortTextField.text];
    } else {
        NSLog(@"Server modified");
        server = [Server sharedInstace];
        server.address = self.serverAddressTextField.text;
        server.port = self.serverPortTextField.text;
    }
}
- (void)prepareConnection {
    [self setUser];
    [self setServer];
    if (self.connectionManager == nil) {
        self.connectionManager = [Connection connectionManager];
//        @weakify(self);
        RAC(self.descriptionLabel, text) = [[RACSignal combineLatest:@[RACObserve(self.connectionManager, isAuthenticated),
                                                                        RACObserve(self.connectionManager, isConnected),
                                                                        RACObserve(self.connectionManager.xmppStream, isDisconnected),]
                                                               reduce:^(NSNumber *authenticated, NSNumber *connected, NSNumber *disconnected){
NSLog(@"RAC: autentificated - %@ | connected - %@ | disconnected - %@", [authenticated stringValue], [connected stringValue], [disconnected stringValue]);
                                                                   BOOL check = (authenticated.boolValue);
                                                                   if (check) {
                                                                       [self stopActivity];
                                                                   }
                                                                   return check ? @"(RAC)": @"Not connected(RAC)";
                                                               }]
                                             deliverOn:[RACScheduler mainThreadScheduler]];

        
    }
}

- (IBAction)testConnectionButtonTapped:(UIButton *)sender {
    [self prepareConnection];
    if (self.connectionManager != nil) {
        NSError *error = [self.connectionManager ready2Connect];
        if (error) {
            self.descriptionLabel.text = [NSString stringWithFormat:@"Error: %@", error.localizedDescription];
        }
    }
}

- (IBAction)loginButtonTapped:(UIButton *)sender {
    [self startActivity];
    [self prepareConnection];
    if (self.connectionManager != nil) {
        NSError *error = [self.connectionManager ready2Connect];
        if (error == nil && self.connectionManager.isAuthenticated) {
        [self performSegueWithIdentifier:@"loginSegue" sender:self];
        } else {
            if (error) {
                self.descriptionLabel.text = [NSString stringWithFormat:@"Error: %@", error.localizedDescription];
            }
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
