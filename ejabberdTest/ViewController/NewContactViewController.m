//
//  NewContactViewController.m
//  ejabberdTest
//
//  Created by Volodymyr Viniarskyi on 2/21/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import "NewContactViewController.h"
#import "Connection.h"
#import "XMPPJID.h"

@interface NewContactViewController ()
@property (weak, nonatomic) IBOutlet UITextField *nickNameTextFiled;
@property (weak, nonatomic) IBOutlet UILabel *infoLable;

@end

@implementation NewContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)addButtonTapped:(UIButton *)sender {
    Connection *connectionManager = [Connection connectionManager];
    XMPPJID *jid = [XMPPJID jidWithString:self.nickNameTextFiled.text];
    [connectionManager.contacts addObject:jid];
    [self.navigationController popViewControllerAnimated:YES];
//    [connectionManager.contacts ]
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
