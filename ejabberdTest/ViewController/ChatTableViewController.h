//
//  ChatTableViewController.h
//  ejabberTest
//
//  Created by Volodymyr Viniarskyi on 2/12/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatTableViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) NSString *userName;
@end
