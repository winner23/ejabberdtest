//
//  ParentViewController.m
//  ejabberdTest
//
//  Created by Volodymyr Viniarskyi on 2/22/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import "ParentViewController.h"

@interface ParentViewController ()
@property (strong, nonatomic) UIView *backgroundView;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@end

@implementation ParentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)startActivity {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.backgroundView removeFromSuperview];
        self.backgroundView = [[UIView alloc] initWithFrame:self.view.frame];
        self.backgroundView.backgroundColor = [UIColor lightGrayColor];
        self.backgroundView.alpha = 0.5;
        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        self.activityIndicator.center = self.view.center;
        if (![self.backgroundView.subviews containsObject:self.activityIndicator]) {
            [self.backgroundView addSubview:self.activityIndicator];
            [self.activityIndicator startAnimating];
            UIView *navigationView = self.navigationController.view;
            if (navigationView) {
                [navigationView addSubview:self.backgroundView];
                [UIView animateWithDuration:0.3 animations:^{
                    [navigationView layoutIfNeeded];
                }];
            } else {
                [self.view addSubview:self.backgroundView];
                [UIView animateWithDuration:0.3 animations:^{
                    [self.view layoutIfNeeded];
                }];
            }
        }
        
    });
}

- (void)stopActivity {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.backgroundView removeFromSuperview];
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        }];
    });
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
