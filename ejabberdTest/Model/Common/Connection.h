//
//  Connection.h
//  ejabberTest
//
//  Created by Volodymyr Viniarskyi on 2/8/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMPPStream.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
@class XMPPStream;
@class XMPPJID;
@interface Connection : NSObject<XMPPStreamDelegate> {
    NSMutableArray *turnSockets;
    BOOL isOpen;
}
@property (strong, nonatomic) XMPPStream *xmppStream;
@property (strong, nonatomic) XMPPJID *jid;
@property (assign, nonatomic) BOOL isConnected;
@property (assign, nonatomic) BOOL isAuthenticated;
@property (strong, nonatomic) NSMutableArray<XMPPJID *> *contacts;
+ (id)connectionManager;
- (NSError *)ready2Connect;
- (void)connect;
- (void)goOnline;
- (void)goOffline;
- (RACSignal *)recieveMsg;
- (void)getAllUsers;
- (void)sendMessage:(NSString *)messageString toUser:(NSString *)userName;
@end
