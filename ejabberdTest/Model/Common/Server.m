//
//  Server.m
//  ejabberTest
//
//  Created by Volodymyr Vinyarskyy on 11.02.2018.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import "Server.h"
@interface Server()
@end

static Server *shared;

@implementation Server

+ (instancetype)sharedInstace {
//    NSAssert(shared != nil, @"[Server]: You should call init first before using the method");
    return shared;
}

+ (instancetype)serverWithAddress:(NSString *)address port:(NSString *)port {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[Server alloc] initWith:address port:port];
    });
    return shared;
}

- (instancetype)initWith:(NSString *)address port:(NSString *)port {
    self = [super init];
    if (self) {
        _address = address;
        _port = port;
    }
    return self;
}

- (NSDictionary *)dictionary {
    NSDictionary *result = @{@"address":self.address,@"port":self.port};
    return result;
}

@end
