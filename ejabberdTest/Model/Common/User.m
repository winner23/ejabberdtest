//
//  User.m
//  ejabberTest
//
//  Created by Volodymyr Viniarskyi on 2/7/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import "User.h"
@interface User() {}
@end

static User *shared = nil;
#define presenceString(enum) [@[@"chat",@"dnd",@"away",@"xa"] objectAtIndex:enum]

@implementation User
+ (instancetype)sharedInstance {
//    NSAssert(shared != nil, @"[User]:You should call init first before using the method");
    return shared;
}

+ (instancetype)userWithJID:(NSString *)jid name:(NSString *)name initPass:(NSString *)pass {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] initWithJID:jid name:name initPass:pass];
    });
    return shared;
}

- (instancetype)initWithJID:(NSString *)jid name:(NSString *)name initPass:(NSString *)pass {
    self = [super init];
    if (self) {
        _name = name;
        _jidString = jid;
        _password = pass;
        _presence = chat;
    }
    return self;
}
- (NSDictionary *_Nonnull)dictionary {
    NSMutableDictionary <NSString*, NSString*> *resultDictionary = [[NSMutableDictionary alloc] initWithCapacity:4];
    resultDictionary[@"name"] = self.name;
    resultDictionary[@"jid"] = self.jidString;
    resultDictionary[@"pass"] = [self encodeStringTo64:_password];
    resultDictionary[@"presence"] = presenceString(self.presence);
    return resultDictionary;
}
#pragma mark - Helpers
- (BOOL) checkPass: (NSString * _Nonnull)word {
    return [word isEqualToString:_password];
}
- (NSString*)encodeStringTo64:(NSString*)fromString {
    NSData *plainData = [fromString dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64String;
    base64String = [plainData base64EncodedStringWithOptions:kNilOptions];
    return base64String;
}
@end
