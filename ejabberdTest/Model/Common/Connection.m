//
//  Connection.m
//  ejabberTest
//
//  Created by Volodymyr Viniarskyi on 2/8/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import "Connection.h"
#import "User.h"
#import "Server.h"
#import "XMPPFramework.h"
#import "XMPPSRVResolver.h"
#import "TURNSocket.h"
#import "XMPPRoster.h"
#import "XMPPRosterMemoryStorage.h"
@interface Connection()
@property (weak, nonatomic) User *user;
@property (weak, nonatomic) Server *server;
@property (weak, nonatomic) RACSignal *recivedMsg;
@property (strong, nonatomic) XMPPRosterMemoryStorage *rosterMemoryStorage;
@end

static Connection *shared;
@implementation Connection
@synthesize xmppStream, user, server, jid;

+ (id)connectionManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    return shared;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        user = [User sharedInstance];
        server = [Server sharedInstace];
        xmppStream = [[XMPPStream alloc] init];
        _contacts = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"users"]];
       // Load list from Defaults
        [self monitor];
        [xmppStream addDelegate:self delegateQueue:dispatch_get_main_queue()];
    }
    return self;
}

- (void)dealloc {
    [[NSUserDefaults standardUserDefaults] setObject:self.contacts forKey:@"users"];
}
- (void)getAllUsers {
    self.rosterMemoryStorage = [[XMPPRosterMemoryStorage alloc] init];
    XMPPRoster *roster = [[XMPPRoster alloc] initWithRosterStorage:self.rosterMemoryStorage];
    [roster addDelegate:self delegateQueue:dispatch_get_main_queue()];
    roster.autoFetchRoster = YES;
    roster.autoAcceptKnownPresenceSubscriptionRequests = YES;
    [roster activate:xmppStream];
    [roster fetchRoster];
}

//- (void)xmppRosterDidEndPopulating:(XMPPRoster *)sender {
//    NSArray *jids = [self.rosterMemoryStorage unsortedUsers];
//    for (XMPPJID *jid in jids) {
//        NSLog(@"%@",jid);
//    }
//}
//- (void)xmppRosterDidChange:(XMPPRosterMemoryStorage *)sender {
//    NSArray *jids = [self.rosterMemoryStorage unsortedUsers];
//    NSLog(@"All users: %lul",(unsigned long)jids.count);
//}
//- (void)xmppRosterDidPopulate:(XMPPRosterMemoryStorage *)sender {
//    NSArray *jids = [self.rosterMemoryStorage unsortedUsers];
//    NSLog(@"All users: %lul",(unsigned long)jids.count);
//}
- (void)monitor {
    RAC(self, isAuthenticated) = RACObserve(self.xmppStream, isAuthenticated);
    RAC(self, isConnected) = RACObserve(self.xmppStream, isConnected);
}

- (void)xmppStream:(XMPPStream *)sender didNotAuthenticate:(NSXMLElement *)error {
    NSLog(@"Did Not Authenticate:%@", error);
}

- (NSError *)ready2Connect {
    jid = [XMPPJID jidWithString:self.user.jidString];
    [self.xmppStream setMyJID:jid];
    NSError *result = nil;
    if (![xmppStream isDisconnected]) {
        return result;
    }
    self.xmppStream.hostName = self.server.address;
    self.xmppStream.hostPort = [self.server.port integerValue];
    [[self xmppStream] connect:&result];
//  Commented for new version
//    [self.xmppStream connectWithTimeout:XMPPStreamTimeoutNone error:&error];
    if (result) {
        NSLog(@"%@", result.localizedDescription);
        return result;
    }
    return result;
}

- (NSError *)login {
    NSError *error = nil;
    if (self.user) {
        [self.xmppStream authenticateWithPassword:user.password error:&error];
    }
    return error;
}
- (void)goOnline {
    XMPPPresence *presenceStatus = [XMPPPresence presenceWithType:@"available"];
    [[self xmppStream] sendElement:presenceStatus];
}
- (void)goOffline {
    XMPPPresence *presenceStatus = [XMPPPresence presenceWithType:@"unavailable"];
    [[self xmppStream] sendElement:presenceStatus];
}
- (void)connect {
    TURNSocket *turnSocket = [[TURNSocket alloc] initWithStream:[self xmppStream] toJID:[self jid]];
    [turnSockets addObject:turnSocket];
    [turnSocket startWithDelegate:self delegateQueue:dispatch_get_main_queue()];
}

- (void)sendMessage:(NSString *)messageString toUser:(NSString *)userName{
    if (messageString.length < 1) {
        NSLog(@"The message size incorrect:%lul",(unsigned long)messageString.length);
        return;
    }
    NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
    [body setStringValue:messageString];
    NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
    [message addAttributeWithName:@"type" stringValue:@"chat"];
    [message addAttributeWithName:@"to" stringValue:userName];
    [message addChild:body];
    [self.xmppStream sendElement:message];
}

#pragma mark - Delegates of XMPPFramework

- (void)xmppStreamDidConnect:(XMPPStream *)sender {
    NSLog(@"%@", sender.isConnected ? @"!!!Connected!!!" : @"Not connected!");
    isOpen = YES;
    NSError *error = nil;
    User *user = [User sharedInstance];
    [self.xmppStream authenticateWithPassword:user.password error:&error];
    if (error) {
        NSLog(@"Authentication error:%@",error.localizedDescription);
        return;
    }
    [self goOnline];
    
}
- (void)xmppRosterDidEndPopulating:(XMPPRoster *)sender{
//    [appDelegate.mContactHandler clearContacts];
    
    NSArray* jids = [self.rosterMemoryStorage unsortedUsers];//[self.rosterMemoryStorage jidsForXMPPStream:self.xmppStream];
//    for (int i=0; i<jids.count; i++)
//    {
//        RosterContact* contact = [[RosterContact alloc]init];
//        contact.jid = [jids objectAtIndex:i];
//        NSLog(@"%@",contact.jid);
//    }
  //  [self postResultNotification:kReload withResult:nil];
}

- (void)xmppStreamDidAuthenticate:(XMPPStream *)sender {
    NSLog(@"Authenticated");
    [self goOnline];
}
- (void)xmppStreamDidDisconnect:(XMPPStream *)sender withError:(NSError *)error {
    NSLog(@"Disconnected: %@", error.localizedDescription);
}
//- (void)xmppStream:(XMPPStream *)sender willSecureWithSettings:(NSMutableDictionary *)settings
//{
//    settings[kCFStreamSSLLevel] = @(YES);
//}
- (BOOL)xmppStream:(XMPPStream *)sender didReceiveIQ:(XMPPIQ *)iq {
    return NO;
}
- (void)xmppStream:(XMPPStream *)sender didReceivePreısence:(XMPPPresence *)presence {
    NSLog(@"Presence changed:%@", presence);
}
- (void)xmppStream:(XMPPStream *)sender didSendPresence:(XMPPPresence *)presence {
    NSLog(@"Presence changed:%@", presence);
}
- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message {
    NSString *messageString = [[message elementForName:@"body"] stringValue];
    NSString *from = [[message attributeForName:@"from"] stringValue];
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setObject:messageString forKey:@"msg"];
    [dictionary setObject:from forKey:@"sender"];
    _recivedMsg = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [subscriber sendNext:dictionary];
        [subscriber sendCompleted];
        return [RACDisposable disposableWithBlock:^{
            [xmppStream disconnect];
        }];
    }];
}
- (RACSignal *)recieveMsg {
    return _recivedMsg;
}

- (void)turnSocket:(TURNSocket *)sender didSucceed:(GCDAsyncSocket *)socket {
    
    NSLog(@"TURN Connection succeeded!");
    NSLog(@"You now have a socket that you can use to send/receive data to/from the other person.");
    
    [turnSockets removeObject:sender];
}

- (void)turnSocketDidFail:(TURNSocket *)sender {
    
    NSLog(@"TURN Connection failed!");
    [turnSockets removeObject:sender];
    
}

@end


