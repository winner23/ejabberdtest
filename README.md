# eJabberd client #

Use XMPPFramework for connection to eJabberd server.

### What is this repository for? ###
The Jabber client for iOS (Objective-C, XMPPFramework, ReactiveCocoa)
* Version   
version 0.0.1

### How do I get set up docker image? ###

#### Initialization Image
* Download prepared archive:
https://bitbucket.org/winner23/ejabberdtest/downloads/ejabber.zip
* Unpack the downloaded archive
to folder *`~/Development/Docker/`

* Folders for sharing resources:   
  *`~/Development/Docker/ejabber/conf`   
  *`~/Development/Docker/ejabber/database`   
  *`~/Development/Docker/ejabber/ssl`   
  *`~/Development/Docker/ejabber/module_source`   

* Download and start image
`docker run -d --name "ejabberd2" -p 5222:5222 -p 5269:5269 -p 5280:5280 -p 4560:4560 -p 5443:5443 \
-v ~/Development/Docker/ejabber/conf:/opt/ejabberd/conf \
-v ~/Development/Docker/ejabber/database:/opt/ejabberd/database \
-v ~/Development/Docker/ejabber/ssl:/opt/ejabberd/ssl \
-v ~/Development/Docker/ejabber/module_source:/opt/ejabberd/module_source \
-h 'xmpp.local.net' -e "XMPP_DOMAIN=local.net" -e "ERLANG_NODE=ejabberd" \
-e "EJABBERD_ADMINS=admin@local.net admin2@local.net" \
-e "EJABBERD_USERS=admin@local.net:pass1234 admin2@local.net:pass4321" \
-e "TZ=Europe/Kiev" rroemhild/ejabberd:latest`
#### Register new user for eJabberd
`docker exec ejabberd2 ejabberdctl register login domain.net password`   
* Example:
`docker exec ejabberd2 ejabberdctl register winner local.net 2311`
